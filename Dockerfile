FROM registry.gitlab.com/final-project25/templates-and-configurations/backend-base-image:latest AS build
COPY . /backend
WORKDIR /backend
RUN chmod +x gradlew && ./gradlew build

FROM registry.gitlab.com/final-project25/templates-and-configurations/backend-base-image:latest AS app
COPY --from=build /backend/build/libs/*jar .
EXPOSE 8080
ENTRYPOINT ["java","-jar","dev-school-app-1.0-SNAPSHOT.jar"]
